This is the nodejs server for finding books and let user to manage the book
they loved.

The main function are:

     /search  query           find books
     /like    bookid token    add book to like list
     /remove  bookid token    remove book from like list
     /list    token           return like list
     /register email password  register user
     /login   email  password  login user
     
