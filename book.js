var restify = require('restify');

var request = require("request");


var fs = require("fs");

var crypto = require('crypto');


var tokens = {};


// store the map from token to username
tokens = require("./tokenInfo.json");

// generate a random token
function generateToken() {
    return crypto.randomBytes(4).toString('hex');
}


// store the search results information
function addToSearchedInfo(items) {
    var searchedInfo = require("./searchedInfo.json");

    for (var i = 0; i < items.length; i++) {
        searchedInfo[items[i].id] = items[i];
    };

    fs.writeFileSync("searchedInfo.json", JSON.stringify(searchedInfo), "utf8");

}

// trans diction to array
function dic2arr(dic) {
    var arr = [];
    for (var key in dic) {
        if (dic.hasOwnProperty(key)) {

            arr.push(dic[key]);
        }
    }

    return arr;
}

// test whether email is valid
function isValid(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

// add username password information
function addUser(email, pass) {

    if (email == undefined) {
        return {
            'success': false,
            'message': 'argument email must be supplied'
        }
    }

    if (pass == undefined) {
        return {
            'success': false,
            'message': 'argument password must be supplied'
        }
    }

    if (!isValid(email)) {
        return {
            'success': false,
            'message': 'email not valid'
        };
    }

    if (pass.length < 4) {
        return {
            'success': false,
            'message': 'password length must at least 4'
        };
    }


    var userInfo = require("./userInfo.json");

    if (userInfo[email] != undefined) {
        return {
            'success': false,
            'message': 'email already registered'
        }
    }

    userInfo[email] = pass;



    fs.writeFileSync("userInfo.json", JSON.stringify(userInfo), "utf8");

    return {
        'success': true,
        'message': 'register success'
    };
}

// process login request
function login(email, pass) {
    if (email == undefined) {
        return {
            'success': false,
            'message': 'argument email must be supplied'
        }
    }

    if (pass == undefined) {
        return {
            'success': false,
            'message': 'argument password must be supplied'
        }
    }

    var userInfo = require("./userInfo.json");
    if (userInfo[email] == undefined) {
        return {
            'success': false,
            'message': 'user does not exist'
        };
    }

    if (userInfo[email] != pass) {
        return {
            'success': false,
            'message': 'password not correct'
        };
    }

    var token = generateToken();
    tokens[token] = email;

    return {
        'success': true,
        'message': 'log in success',
        'token': token
    };
}


function addLike(email, bookid) {
    var userInfo = require("./userInfo.json");
    var searchedInfo = require("./searchedInfo.json");
    var likeInfo = require("./likeInfo.json");

    if (userInfo[email] == undefined) {

        return {
            'success': false,
            'message': "user doesn't exist"
        };
    }

    if (searchedInfo[bookid] == undefined) {
        return {
            'success': false,
            'message': "no such book"
        };
    }


    if (likeInfo[email] == undefined) {
        likeInfo[email] = {};
    }

    if (likeInfo[email][bookid] != undefined) {
        return {
            'success': false,
            'message': "already in the like list"
        };
    }


    likeInfo[email][bookid] = "";

    // likeInfo[email][bookid] = searchedInfo[bookid];

    fs.writeFileSync("likeInfo.json", JSON.stringify(likeInfo), "utf8");

    var data = listLike(likeInfo[email], searchedInfo);

    return {
        'success': true,
        'message': "add successfully",
        'data': data
    };

}

function removeLike(email, bookid) {
    var userInfo = require("./userInfo.json");
    var searchedInfo = require("./searchedInfo.json");
    var likeInfo = require("./likeInfo.json");

    if (userInfo[email] == undefined) {

        return {
            'success': false,
            'message': "user doesn't exist"
        };
    }


    if (likeInfo[email] == undefined || likeInfo[email][bookid] == undefined) {
        return {
            'success': false,
            'message': "no such book in like list"
        };
    }

    delete likeInfo[email][bookid];

    fs.writeFileSync("likeInfo.json", JSON.stringify(likeInfo), "utf8");

    var data = listLike(likeInfo[email], searchedInfo);

    return {
        'success': true,
        'message': "remove successfully",
        'data': data
    };

}



function search(query) {

    if (query == undefined) {

        return {
            'success': false,
            'message': 'argument query must be supplied'
        };

    }

    // query = req.body.query;


    var url = "https://www.googleapis.com/books/v1/volumes?q=" + query;


    request({
        url: url,
        json: true
    }, function(error, response, body) {

        if (!error && response.statusCode === 200) {

            var data = body.items;

            addToSearchedInfo(data);


            return {
                'success': true,
                'message': "search successfully",
                'data': data
            };

        } else {

            return {
                'success': false,
                'message': "search failed"
            };
        }
    });

}




function registerHandle(req, res, next) {


    console.log([req.body.email, req.body.password]);
    var ans = addUser(req.body.email, req.body.password);
    res.send(ans);
}


function loginHandle(req, res, next) {

    console.log([req.body.email, req.body.password]);

    var ans = login(req.body.email, req.body.password);
    res.send(ans);
}




function likeHandle(req, res, next) {

    if (req.body.token == undefined) {

        return next(new restify.InvalidArgumentError('no token argument'));
    }

    var email = tokens[req.body.token];

    var ans;
    if (email == undefined) {

        ans = {
            'success': false,
            'message': "invalid token, please log in"
        };
    } else {
        var bookid = req.body.bookid;

        ans = addLike(email, bookid);
    }

    res.send(ans);

}


function removeLikeHandle(req, res, next) {

    if (req.body.token == undefined) {

        return next(new restify.InvalidArgumentError('no token argument'));
    }

    var email = tokens[req.body.token];
    var ans;
    if (email == undefined) {

        ans = {
            'success': false,
            'message': "invalid token"
        };
    } else {
        var bookid = req.body.bookid;

        ans = removeLike(email, bookid);
    }

    res.send(ans);
}


function listLike(idlist, searchedInfo) {
    var arr = [];
    for (var key in idlist) {
        if (idlist.hasOwnProperty(key)) {

            arr.push(searchedInfo[key]);
        }
    }

    return arr;
}


function listLikeFor(email) {

    var searchedInfo = require("./searchedInfo.json");
    var likeInfo = require("./likeInfo.json");

    if (likeInfo[email] == undefined) {
        likeInfo[email] = {};
    }

    var data = listLike(likeInfo[email], searchedInfo);

    return {
        'success': true,
        'message': "list like successfully",
        'data': data
    };

}




function listHandle(req, res, next) {

    if (req.body.token == undefined) {

        return next(new restify.InvalidArgumentError('no token argument'));
    }

    var email = tokens[req.body.token];
    var ans;
    if (email == undefined) {

        ans = {
            'success': false,
            'message': "invalid token"
        };
    } else {

        ans = listLikeFor(email);
    }

    res.send(ans);
}




function searchHandle(req, res, next) {
    // query = req.body.query;

    var query = req.body.query;

    if (query == undefined) {

        res.send({
            'success': false,
            'message': 'argument query must be supplied'
        });


        return next();

    }



    var url = "https://www.googleapis.com/books/v1/volumes?q=" + query;


    request({
        url: url,
        json: true
    }, function(error, response, body) {

        if (!error && response.statusCode === 200) {

            var data = body.items;

            addToSearchedInfo(data);


            res.send({
                'success': true,
                'message': "search successfully",
                'data': data
            });

            return next();

        } else {

            res.send({
                'success': false,
                'message': "search failed"
            });
            return next();

        }
    });


}



var bookServer = restify.createServer();

bookServer.use(restify.queryParser());
bookServer.use(restify.bodyParser({
    mapParams: false
}));


bookServer.post('/search', searchHandle);
bookServer.post('/like', likeHandle);
bookServer.post('/remove', removeLikeHandle);
bookServer.post('/list', listHandle);
bookServer.post('/register', registerHandle);
bookServer.post('/login', loginHandle);


bookServer.get(/\/script(\/.*)*?/, restify.serveStatic({
    directory: './client',
    default: "book.html"
}));
bookServer.get('/', restify.serveStatic({
    directory: './client',
    default: "book.html"
}));


bookServer.listen(process.env.PORT || 3800, function() {
    console.log(bookServer.url);
});

